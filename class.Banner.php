<?php


class Banner {

	private $source = "banner2.jpg";

	
	public function CreateBanner($data) {
		
		
		$game_days_pvp = floor($data['data']['pvp']['totalBattleTime']/1000/60/60/24);
		$game_hour_pvp = floor(($data['data']['pvp']['totalBattleTime'] - $game_days_pvp*1000*60*60*24)/1000/60/60);
		$game_minutes_pvp = floor(($data['data']['pvp']['totalBattleTime'] - $game_days_pvp*1000*60*60*24 - $game_hour_pvp*1000*60*60)/1000/60);
		$game_sec_pvp = floor(($data['data']['pvp']['totalBattleTime'] - $game_days_pvp*1000*60*60*24 - $game_hour_pvp*1000*60*60 - $game_minutes_pvp*1000*60)/1000); 
		$game_msec_pvp = floor(($data['data']['pvp']['totalBattleTime'] - $game_days_pvp*1000*60*60*24 - $game_hour_pvp*1000*60*60 - $game_minutes_pvp*1000*60 - $game_sec_pvp*1000));
		$kd = $data['data']['pvp']['totalKill']/$data['data']['pvp']['totalDeath'];
		
		$timePerBattle = $data['data']['pvp']['totalBattleTime']/$data['data']['pvp']['gamePlayed'];
		$game_per_minutes_pvp = floor(($timePerBattle)/1000/60);
		$game_per_sec_pvp  = floor(($timePerBattle- $game_per_minutes_pvp*1000*60)/1000);
		
		$image  	= imagecreatefromjpeg($this->source);
		
		$Font	= '24Janvier-Light.ttf';
				
		$color	= imagecolorallocate($image, 255, 255, 225);
		$shadow = imagecolorallocate($image, 33, 33, 33);
		
		$POS = array(array(15, 0, 50, 31), array(15, 0, 50, 30), array(15, 0, 190, 31));
	//	var_dump($POS);
		
		$wl	= $data['data']['pvp']['gameWin'] / ($data['data']['pvp']['gamePlayed'] - $data['data']['pvp']['gameWin']);
		
		
		imagettftext($image, 15, 0, 50, 31, $shadow, $Font, $data['data']['clan']['tag'].' '.$data['data']['nickName']);
		imagettftext($image, 15, 0, 50, 30, $color, $Font, $data['data']['clan']['tag'].' '.$data['data']['nickName']);
		
	//	imagettftext($image, 14, 0, 180, 31, $shadow, $Font, 'Kämpfe: '.$data['data']['pvp']['gamePlayed']);
	//	imagettftext($image, 14, 0, 180, 30, $color, $Font, 'Kämpfe: '.$data['data']['pvp']['gamePlayed']);
		
		imagettftext($image, 14, 0, 180, 31, $shadow, $Font, 'KD: '.number_format($kd, 1));
		imagettftext($image, 14, 0, 180, 30, $color, $Font, 'KD: '.number_format($kd, 1));
		
		imagettftext($image, 14, 0, 180, 56, $shadow, $Font, 'Wins: '.$data['data']['pvp']['gameWin']);
		imagettftext($image, 14, 0, 180, 55, $color, $Font, 'Wins: '.$data['data']['pvp']['gameWin']);
		
		imagettftext($image, 14, 0, 180, 81, $shadow, $Font, 'W/L-Ratio: '.number_format($wl, 2));
		imagettftext($image, 14, 0, 180, 80, $color, $Font, 'W/L-Ratio: '.number_format($wl, 2));
		
		imagettftext($image, 14, 0, 50, 56, $shadow, $Font, "Karma: " .$data['data']['karma']);
		imagettftext($image, 14, 0, 50, 55, $color, $Font, "Karma: " .$data['data']['karma']);
				
		imagettftext($image, 14, 0, 50, 81, $shadow, $Font, "Elo: " .number_format($data['data']['effRating'], 0,'.',''));
		imagettftext($image, 14, 0, 50, 80, $color, $Font, "Elo: " .number_format($data['data']['effRating'], 0,'.',''));	

	//	imagettftext($image, 14, 0, 50, 81, $color, $Font, "" .number_format($game_days_pvp, 0)."d ".number_format($game_hour_pvp, 0)."h ".number_format($game_minutes_pvp, 0)."min ".number_format($game_sec_pvp, 0)."sec");
	//	imagettftext($image, 14, 0, 50, 80, $color, $Font, "" .number_format($game_days_pvp, 0)."d ".number_format($game_hour_pvp, 0)."h ".number_format($game_minutes_pvp, 0)."min ".number_format($game_sec_pvp, 0)."sec");
	//	imagettftext($image, 14, 0, 50, 81, $color, $Font, "" .number_format($game_per_minutes_pvp, 0)."min ".number_format($game_per_sec_pvp, 0)."sec");
	//	imagettftext($image, 14, 0, 50, 80, $color, $Font, "" .number_format($game_per_minutes_pvp, 0)."min ".number_format($game_per_sec_pvp, 0)."sec");
		
		if (!isset($_GET["karma"]) || !$_GET["karma"] === true) {
			$t = "T";
		} else {
			$this->Karma($data['data']['karma'], $image);
		}
		
		if(!isset($_GET['debug']))
		{
				header('Content-type: image/jpg');
			
		}else{
			if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
				$ip = $_SERVER['HTTP_CLIENT_IP'];
			} elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
				$ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
			} else {
				$ip = $_SERVER['REMOTE_ADDR'];
			}
			if($ip != "127.0.0.1"){
				
				header('Content-type: image/jpg');
			}
		}
		
		
		ImageJPEG($image);
		imagedestroy($image);
	}

	public function Karma( $karma = 0, $image) {
		$karma = -1;
		if ($karma >= 0) {
			$blue = imagecreatefromjpeg("blue.jpg");
			$blue_dark = imagecreatefromjpeg("blue_d.jpg");
			
			$marge_right = 10;
			$marge_bottom = 10;
			$sx = imagesx($blue);
			$sy = imagesy($blue);
			
			ImageCopy($image, $blue, 0, 0, 0, 0, 5, 0, 0);
			
			imagedestroy($blue);
			imagedestroy($blue_dark);
			imagettftext($image, 18, 0, 50, 80, imagecolorallocate($image, 255, 255, 225), '24Janvier-Light.ttf', "t");
		} else {
			
			
			
			
			
		}
	}
}
